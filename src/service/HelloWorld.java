/**
 * 
 */
package service;

import java.util.ArrayList;

import javax.annotation.security.RolesAllowed;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import model.User;

/**
 * @author Ozgur Bircan
 *
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface HelloWorld {
	public  ArrayList<User> getUsers();
	public User findUserByusername(@WebParam(name="id") int id);
	/**
	 * @param name
	 * @param lastname
	 * @param id
	 * @return
	 */
	String addUser(@WebParam(name="name") String name, @WebParam(name="lastname") String lastname,@WebParam(name="id") int id);

}