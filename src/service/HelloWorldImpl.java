/**
 * 
 */
package service;

import java.util.ArrayList;

import javax.jws.WebService;

import model.User;

/**
 * @author Ozgur Bircan
 *
 */
@WebService(endpointInterface = "service.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

	/*
	 * (non-Javadoc)
	 * 
	 * @see service.HelloWorld#getUsers()
	 */
	ArrayList<User> user = new ArrayList<User>();

	@Override
	public ArrayList<User> getUsers() {

		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see service.HelloWorld#addUser(java.lang.String)
	 */
	@Override
	public String addUser(String name,String lastname, int id) {
		
		if (name.isEmpty() || name.equals(null)) 
		{
		return "failed";	
		}
		
		System.out.println("addUser called");
		User userModel = new User();
		userModel.setId(id);
		userModel.setName(name);
		userModel.setLastname(lastname);
		user.add(userModel);

		return "Success";
	}

	/* (non-Javadoc)
	 * @see service.HelloWorld#findUserByusername(java.lang.String)
	 */
	@Override
	public User findUserByusername(int id) {
		// TODO Auto-generated method stub
		
		for (User user2 : user) {
			if (user2.getId()==id) 
			{
				return user2;
			}
		
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see service.HelloWorld#addUser(java.lang.String)
	 */
	

}